﻿using Rabbit.ClientService.Models;
using System.Diagnostics.CodeAnalysis;

namespace RabbitMQ.Fakes.models
{
	[ExcludeFromCodeCoverage]
	public class ExchangeQueueBinding
    {
		public ExchangeQueueBinding()
		{

		}
		
		public ExchangeQueueBinding(AmqpChannelSetting amqpChannel)
		{
			Exchange = new Exchange(amqpChannel.Exchange.Name, amqpChannel.Exchange.Type, amqpChannel.Exchange.Durable);
			RoutingKey = amqpChannel.RoutingKey;
			Queue = new Queue(amqpChannel.Queue.Name, amqpChannel.Queue.AutoDelete);
		}

		public string RoutingKey { get; set; }

        public Exchange Exchange { get; set; }

        public Queue Queue { get; set; }

        public string Key
        {
            get { return string.Format("{0}|{1}|{2}", Exchange.Name, RoutingKey, Queue.Name); }
        }
    }
}