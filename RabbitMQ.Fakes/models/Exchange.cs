﻿using Rabbit.ClientService.Models;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;

namespace RabbitMQ.Fakes.models
{
	[ExcludeFromCodeCoverage]
	public class Exchange : AmqpExchangeSetting
	{
        public IDictionary Arguments = new Dictionary<string, object>();

        public ConcurrentQueue<RabbitMessage> Messages = new ConcurrentQueue<RabbitMessage>();
        public ConcurrentDictionary<string,ExchangeQueueBinding> Bindings = new ConcurrentDictionary<string,ExchangeQueueBinding>();

		public Exchange(string name, string type, bool autoDelete) : base(name, type, autoDelete)
		{
		}

		public Exchange(string name, string type, bool autoDelete, bool durable) : base(name, type, autoDelete, durable)
		{
		}

		public void PublishMessage(RabbitMessage message)
        {
            this.Messages.Enqueue(message);

            if (string.IsNullOrWhiteSpace(message.RoutingKey))
            {
                foreach (var binding in Bindings)
                {
                    binding.Value.Queue.PublishMessage(message);
                }
            }
            else
            {
                var matchingBindings = Bindings
                    .Values
                    .Where(b => b.RoutingKey == message.RoutingKey);

                foreach (var binding in matchingBindings)
                {
                    binding.Queue.PublishMessage(message);
                }
            }
        }
    }
}