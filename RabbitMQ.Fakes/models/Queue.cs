﻿using Rabbit.ClientService.Models;
using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace RabbitMQ.Fakes.models
{
	[ExcludeFromCodeCoverage]
	public class Queue : AmqpQueueSetting
	{
        //public string Name { get; set; }

        //public bool IsDurable { get; set; }

        public bool IsExclusive { get; set; }

        //public bool IsAutoDelete { get; set; }

        public IDictionary Arguments = new Dictionary<string, object>();

        public ConcurrentQueue<RabbitMessage> Messages = new ConcurrentQueue<RabbitMessage>();
        public ConcurrentDictionary<string,ExchangeQueueBinding> Bindings = new ConcurrentDictionary<string,ExchangeQueueBinding>();

        public event EventHandler<RabbitMessage> MessagePublished = (sender, message) => { };
		
		public Queue(string name, bool autoDelete) : base(name, autoDelete)
		{
		}

		public Queue(string name, bool autoDelete, bool durable) : base(name, autoDelete, durable)
		{
		}

		public void PublishMessage(RabbitMessage message)
        {
            var queueMessage = message.Copy();
            queueMessage.Queue = this.Name;

            this.Messages.Enqueue(queueMessage);

            MessagePublished(this, queueMessage);
        }

		public void SubscribeClean()
		{
			MessagePublished = (sender, message) => { };
		}
    }
}