﻿using System.Collections.Concurrent;
using System.Diagnostics.CodeAnalysis;
using RabbitMQ.Fakes.models;

namespace RabbitMQ.Fakes
{
	[ExcludeFromCodeCoverage]
	public class RabbitServer
	{
		public ConcurrentDictionary<string, FakeConnection> Connections = new ConcurrentDictionary<string, FakeConnection>();
		private ConcurrentDictionary<string, Exchange> _exchanges = new ConcurrentDictionary<string, Exchange>();
		private ConcurrentDictionary<string, models.Queue> _queues = new ConcurrentDictionary<string, models.Queue>();

		public ConcurrentDictionary<string, Exchange> Exchanges
		{
			get { return IsWorking ? _exchanges : null; }
		}

		public ConcurrentDictionary<string, models.Queue> Queues
		{
			get { return IsWorking ? _queues : null; }
		}

		public bool IsWorking { get; private set; } = true;

        public void Reset()
        {
            Exchanges.Clear();
            Queues.Clear();
        }

		public void DisconnectOfException()
		{
			IsWorking = false;
			foreach(var ex in _queues)
			{
				ex.Value.SubscribeClean();
			}
			foreach(var con in Connections)
			{
				con.Value.OnException();
			}
		}

		public void Connect()
		{
			IsWorking = true;
		}
	}
}