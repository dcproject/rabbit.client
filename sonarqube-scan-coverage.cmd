REM# Rebuild solution
dotnet build Rabbit.sln /t:Rebuild

REM# Generate all reports
dotnet test Rabbit.ClientService.Test\Rabbit.ClientService.Test.csproj -v n /p:CollectCoverage=true /p:CoverletOutputFormat=opencover /p:CoverletOutput=coverage
dotnet build-server shutdown

REM# Initialize sonar-scaner 
dotnet-sonarscanner begin /k:"Rabbit.ClientServicce" /d:sonar.cs.opencover.reportsPaths="Rabbit.ClientService.Test\coverage.opencover.xml" /d:sonar.coverage.exclusions="**Test*.cs"
REM# Build solution by sonar-scaner
dotnet build Rabbit.sln /t:Rebuild
REM# Build all test projects by sonar-scaner
dotnet build Rabbit.ClientService.Test/Rabbit.ClientService.Test.csproj /t:Rebuild
dotnet-sonarscanner end 

pause
