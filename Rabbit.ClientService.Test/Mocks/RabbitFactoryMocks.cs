﻿using Microsoft.Extensions.Logging;
using Rabbit.ClientService.Models;
using RabbitMQ.Client;
using RabbitMQ.Fakes;
using System.Diagnostics.CodeAnalysis;
using Tools.UnitTest.Fakes;

namespace Rabbit.ClientService.Test.Mocks
{
	[ExcludeFromCodeCoverage]
	internal class RabbitFactoryMocks : Factory<FakeConnectionFactory>
	{
		public FakeConnectionFactory FakeFactory { get; set; } = null;

		public RabbitFactoryMocks(RabbitServer service, IConnection connection) : this()
		{
			FakeFactory = new FakeConnectionFactory().WithRabbitServer(service).WithConnection(connection);
		}

		public RabbitFactoryMocks() : this(new LoggerFake<FakeConnectionFactory>())
		{
		}

		public RabbitFactoryMocks(ILogger logger) : base(logger)
		{
		}

		protected override IConnectionFactory CreateConnectFactory(AmqpHostSetting setting)
		{
			return FakeFactory = (FakeFactory ?? base.CreateConnectFactory(setting) as FakeConnectionFactory);
		}

		//protected override ISubscribeApplier CreateSubscribeApplier()
		//{
		//	return new SubscribeApplier(FakeFactory);
		//}
	}

	//class SubscribeApplier : ISubscribeApplier
	//{
	//	private FakeConnectionFactory _fakeFactory { get; set; } = null;

	//	public SubscribeApplier(FakeConnectionFactory fakeFactory)
	//	{
	//		_fakeFactory = fakeFactory;
	//	}

	//	public void Binding(IModel channel, Subscriber subscriber)
	//	{
	//		var consumer = new EventingBasicConsumer(channel);
	//		consumer.Received += subscriber.Receiving;
	//		channel.BasicConsume(
	//				queue: subscriber.ChannelSetting.Queue.Name,
	//				consumerTag: subscriber.ConsumerTag,
	//				autoAck: false,
	//				consumer: consumer
	//			);
	//	}
	//}
}
