﻿using Rabbit.ClientService.Models;
using Rabbit.ClientService.Test.Mocks;
using RabbitMQ.Fakes.models;
using System.Collections.Generic;
using System.Threading.Tasks;
using Tools.UnitTest.Extension;
using Tools.UnitTest.Fakes;
using Xunit;

namespace Rabbit.ClientService.Test.Tests
{
	public class ChannelTest
	{
		AmqpHostSetting HostSetting
		{
			get
			{
				return new AmqpHostSetting(hostName: "test-host-1", port: 1111, userName: "test-user", password: "test-pass", requestedHeartbeat: 1);
			}
		}

		AmqpChannelSetting ChannelSetting
		{
			get
			{
				return new AmqpChannelSetting(
					exchange: new AmqpExchangeSetting(name: "ex-ch", type: "Fanout", autoDelete: false),
					routingKey: "*",
					queue: new AmqpQueueSetting(name: "q", autoDelete: false));
			}
		}

		[Fact]
		public async Task JustCheckResult_CannelRegistered_Test()
		{
			var testMessage = "Test message is ok";

			var rabbitMock = new RabbitFactoryMocks();
			IRabbitClient rabbitClient = new RabbitHost(rabbitMock, new LoggerFake<RabbitHost>());

			IPublishChannel channel = null;
			await Task.Delay(0).Delay(() =>
			{
				//await open Channel
				channel = rabbitClient.Channel(HostSetting,
					new Dictionary<string, AmqpChannelSetting>() { { testMessage.GetType().Name, ChannelSetting } });
				return channel != null;
			}, 1000);
			Assert.NotNull(channel);
		}

		[Fact]
		public async Task Exchange_IsRegistered_Test()
		{
			var testMessage = "Test message is ok";
			var rabbitMock = new RabbitFactoryMocks();
			IRabbitClient rabbitClient = new RabbitHost(rabbitMock, new LoggerFake<RabbitHost>());

			Exchange actualExchange = null;
			IPublishChannel channel = null;
			await Task.Delay(0).Delay(() => {
				//await open Channel
				channel = rabbitClient.Channel(HostSetting,
					new Dictionary<string, AmqpChannelSetting>() { { testMessage.GetType().Name, ChannelSetting } });
				return channel != null;
			}, 1000);
			channel.Publish(testMessage);
			await Task.Delay(0).Delay(() =>
			{
				//await exist Exchange
				rabbitMock.FakeFactory.Server.Exchanges.TryGetValue(ChannelSetting.Exchange.Name, out actualExchange);
				return actualExchange != null;
			}, 1000);
			Assert.NotNull(actualExchange);
		}

		[Fact]
		public async Task Exchange_RegisteredIsProved_Test()
		{
			var testMessage = "Test message is ok";
			var rabbitMock = new RabbitFactoryMocks();
			IRabbitClient rabbitClient = new RabbitHost(rabbitMock, new LoggerFake<RabbitHost>());

			Exchange actualExchange = null;
			IPublishChannel channel = null;
			await Task.Delay(0).Delay(() =>
			{
				//await open Channel
				channel = rabbitClient.Channel(HostSetting,
					new Dictionary<string, AmqpChannelSetting>() { { testMessage.GetType().Name, ChannelSetting } });
				return channel != null;
			}, 1000);
			channel.Publish(testMessage);
			await Task.Delay(0).Delay(() =>
			{
				rabbitMock.FakeFactory.Server.Exchanges.TryGetValue(ChannelSetting.Exchange.Name, out actualExchange);
				return actualExchange == ChannelSetting.Exchange;
			}, 1000);
			Assert.Equal(ChannelSetting.Exchange, actualExchange);
		}

		[Fact]
		public async Task Queue_IsRegistred_Test()
		{
			var testMessage = "Test message is ok";
			var rabbitMock = new RabbitFactoryMocks();
			IRabbitClient rabbitClient = new RabbitHost(rabbitMock, new LoggerFake<RabbitHost>());

			Queue actualQueue = null;
			IPublishChannel channel = null;
			await Task.Delay(0).Delay(() =>
			{
				//await open Channel
				channel = rabbitClient.Channel(HostSetting,
					new Dictionary<string, AmqpChannelSetting>() { { testMessage.GetType().Name, ChannelSetting } });
				return channel != null;
			}, 1000);
			channel.Publish(testMessage);
			await Task.Delay(0).Delay(() =>
			{
				rabbitMock.FakeFactory.Server.Queues.TryGetValue(ChannelSetting.Queue.Name, out actualQueue);
				return actualQueue != null;
			}, 1000);
			Assert.NotNull(actualQueue);
		}

		[Fact]
		public async Task Queue_RegistredIsProved_Test()
		{ 
			var testMessage = "Test message is ok";
			var rabbitMock = new RabbitFactoryMocks();
			IRabbitClient rabbitClient = new RabbitHost(rabbitMock, new LoggerFake<RabbitHost>());

			Queue actualQueue = null;
			IPublishChannel channel = null;
			await Task.Delay(0).Delay(() =>
			{
				//await open Channel
				channel = rabbitClient.Channel(HostSetting,
					new Dictionary<string, AmqpChannelSetting>() { { testMessage.GetType().Name, ChannelSetting } });
				return channel != null;
			}, 1000);
			channel.Publish(testMessage);
			await Task.Delay(0).Delay(() =>
			{
				rabbitMock.FakeFactory.Server.Queues.TryGetValue(ChannelSetting.Queue.Name, out actualQueue);
				return ChannelSetting.Queue == actualQueue;
			}, 1000);
			Assert.Equal(ChannelSetting.Queue, actualQueue);
		}

		[Fact]
		public async Task QueueBind_IsRegistred_Test()
		{
			var testMessage = "Test message is ok";
			var rabbitMock = new RabbitFactoryMocks();
			IRabbitClient rabbitClient = new RabbitHost(rabbitMock, new LoggerFake<RabbitHost>());

			ExchangeQueueBinding actualBound = null;
			IPublishChannel channel = null;
			await Task.Delay(0).Delay(() =>
			{
				//await open Channel
				channel = rabbitClient.Channel(HostSetting,
					new Dictionary<string, AmqpChannelSetting>() { { testMessage.GetType().Name, ChannelSetting } });
				return channel != null;
			}, 1000);
			channel.Publish(testMessage);
			await Task.Delay(0).Delay(() =>
			{
				rabbitMock.FakeFactory.Server.Exchanges.TryGetValue(ChannelSetting.Exchange.Name, out Exchange exchange);
				exchange.Bindings.TryGetValue(new ExchangeQueueBinding(ChannelSetting).Key, out actualBound);
				return actualBound != null;
			}, 1000);
			Assert.NotNull(actualBound);
		}

		[Fact]
		public async Task QueueBind_RegistredIsProved_Test()
		{
			var testMessage = "Test message is ok";
			var rabbitMock = new RabbitFactoryMocks();
			IRabbitClient rabbitClient = new RabbitHost(rabbitMock, new LoggerFake<RabbitHost>());

			ExchangeQueueBinding actualBound = null;
			IPublishChannel channel = null;
			await Task.Delay(0).Delay(() =>
			{
				//await open Channel
				channel = rabbitClient.Channel(HostSetting,
					new Dictionary<string, AmqpChannelSetting>() { { testMessage.GetType().Name, ChannelSetting } });
				return channel != null;
			}, 1000);
			channel.Publish(testMessage);
			await Task.Delay(0).Delay(() =>
			{
				rabbitMock.FakeFactory.Server.Exchanges.TryGetValue(ChannelSetting.Exchange.Name, out Exchange exchange);
				exchange.Bindings.TryGetValue(new ExchangeQueueBinding(ChannelSetting).Key, out actualBound);
				return actualBound.RoutingKey == ChannelSetting.RoutingKey;
			}, 1000);
			Assert.Equal(ChannelSetting.RoutingKey, actualBound.RoutingKey);
		}
	}
}
