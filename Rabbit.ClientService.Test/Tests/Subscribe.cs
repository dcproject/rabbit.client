﻿using Rabbit.ClientService.Models;
using Rabbit.ClientService.Test.Mocks;
using RabbitMQ.Fakes.models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Tools.UnitTest.Extension;
using Tools.UnitTest.Fakes;
using Xunit;

namespace Rabbit.ClientService.Test.Tests
{
	public class Subscribe
	{
		AmqpHostSetting HostSetting
		{
			get
			{
				return new AmqpHostSetting(hostName: "test-host-3", port: 1111, userName: "test-user", password: "test-pass", requestedHeartbeat: 1);
			}
		}

		AmqpChannelSetting ChannelSetting
		{
			get
			{
				return new AmqpChannelSetting(
					exchange: new AmqpExchangeSetting(name: "ex-s", type: "Fanout", autoDelete: false),
					routingKey: "*",
					queue: new AmqpQueueSetting(name: "q", autoDelete: false));
			}
		}

		[Fact]
		public async Task BeforeMessage_Test()
		{
			var testMessage = "Test message is ok";
			var rabbitMock = new RabbitFactoryMocks();
			IRabbitClient rabbitClient = new RabbitHost(rabbitMock, new LoggerFake<RabbitHost>());

			string actualMessage = null;
			IPublishChannel channel = null;
			await Task.Delay(0).Delay(() =>
			{
				//await open Channel
				channel = rabbitClient.Channel(HostSetting,
					new Dictionary<string, AmqpChannelSetting>() { { testMessage.GetType().Name, ChannelSetting } });
				return channel != null;
			}, 1000);
			rabbitClient.RegisterSubscribe<String>(HostSetting, (value) =>
			{
				actualMessage = value.Value;
				return Task.Run(() => { return CommunicationStatus.Successfully; });
			});

			channel.Publish(testMessage);
			await Task.Delay(0).Delay(() => { return testMessage == actualMessage; }, 1000);

			Assert.Equal(testMessage, actualMessage);
		}

		[Fact]
		public async Task AfterMassage_Test()
		{
			var testMessage = "Test message is ok";
			var rabbitMock = new RabbitFactoryMocks();
			IRabbitClient rabbitClient = new RabbitHost(rabbitMock, new LoggerFake<RabbitHost>());

			string actualMessage = null;
			IPublishChannel channel = null;
			await Task.Delay(0).Delay(() =>
			{
				//await open Channel
				channel = rabbitClient.Channel(HostSetting,
					new Dictionary<string, AmqpChannelSetting>() { { testMessage.GetType().Name, ChannelSetting } });
				return channel != null;
			}, 1000);
			channel.Publish(testMessage);
			rabbitClient.RegisterSubscribe<String>(HostSetting, (value) =>
			{
				actualMessage = value.Value;
				return Task.Run(() => { return CommunicationStatus.Successfully; });
			});
			await Task.Delay(0).Delay(() => { return testMessage == actualMessage; }, 1000);

			Assert.Equal(testMessage, actualMessage);
		}


		//[Fact]
		//public async Task Reconnecting_Test()
		//{
		//	var testMessage = "Test message is ok";
		//	var hostSetting = HostSetting;
		//	hostSetting.RetriesDelay = 200;
		//	hostSetting.RetriesCount = 9999999;

		//	var rabbitMock = new RabbitFactoryMocks();
		//	IRabbitClient rabbitClient = new RabbitHost(rabbitMock, new LoggerFake<RabbitHost>());
		//	IPublishChannel channel = rabbitClient.Channel(hostSetting,
		//		new Dictionary<string, AmqpChannelSetting>() { { testMessage.GetType().Name, ChannelSetting } });

		//	string actualMessage = null;
		//	//Subscribe
		//	rabbitClient.RegisterSubscribe<String>(HostSetting, (value) =>
		//	{
		//		actualMessage = value.Value;
		//		return Task.Run(() => { return CommunicationStatus.Successfully; });
		//	});
		//	await Task.Delay(0).Delay(() =>
		//	{
		//		//await create Queue
		//		rabbitMock.FakeFactory.Server.Queues.TryGetValue(ChannelSetting.Queue.Name, out Queue actualQueue);
		//		return actualQueue != null;
		//	}, 1000);
		//	//Disconnect
		//	rabbitMock.FakeFactory.Server.DisconnectOfException();
		//	await Task.Delay(0).Delay(() =>
		//	{
		//		//await falsy pablishing
		//		return channel.Publish(testMessage) == CommunicationStatus.FailingDelivery;
		//	}, 1000);
		//	//Reconnect 
		//	rabbitMock.FakeFactory.Server.Connect();
		//	await Task.Delay(0).Delay(() =>
		//	{
		//		//await truthy publishing
		//		return channel.Publish(testMessage) == CommunicationStatus.Successfully;
		//	}, 1000).Delay(() =>
		//	{
		//		//await reciving of subscribe
		//		return testMessage == actualMessage;
		//	}, 1000);

		//	Assert.Equal(testMessage, actualMessage);
		//}

	}
}
