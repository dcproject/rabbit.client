﻿using Rabbit.ClientService.Models;
using Rabbit.ClientService.Test.Mocks;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Tools.UnitTest.Extension;
using Tools.UnitTest.Fakes;
using Xunit;

namespace Rabbit.ClientService.Test.Tests
{
	public class HostTest
	{
		private AmqpHostSetting _hostSetting
		{
			get
			{
				return new AmqpHostSetting(hostName: "test-host-Test", port: 1111, userName: "test-user", password: "test-pass", requestedHeartbeat: 1);
			}
		}

		private AmqpChannelSetting _channelSetting
		{
			get
			{
				return new AmqpChannelSetting(
					exchange: new AmqpExchangeSetting(name: "ex-p", type: "Fanout", autoDelete: false),
					routingKey: "*",
					queue: new AmqpQueueSetting(name: "q", autoDelete: false));
			}
		}

		[Fact]
		public void JustCreatedHost_Test()
		{ 
			var rabbitMock = new RabbitFactoryMocks();
			Assert.NotNull(new RabbitHost(rabbitMock, new LoggerFake<RabbitHost>()));
		}
		
		[Fact]
		public void RabbitHost_GetChannel_AffterDispose()
		{
			var factoryMock = new RabbitFactoryMocks();
			IRabbitClient rabbitClient = new RabbitHost(factoryMock, new LoggerFake<RabbitHost>());
			(rabbitClient as RabbitHost).Dispose();
			Action actual = () => rabbitClient.Channel(new AmqpHostSetting() { });
			Assert.Throws<ObjectDisposedException>(actual);
		}

		[Fact]
		public void RabbitHost_AppChannelSettings_AffterDispose()
		{
			var testMessage = "Test message is ok";
			var factoryMock = new RabbitFactoryMocks();
			IRabbitClient rabbitClient = new RabbitHost(factoryMock, new LoggerFake<RabbitHost>());
			(rabbitClient as RabbitHost).Dispose();
			Action actual = () => rabbitClient.Channel(new AmqpHostSetting() { },
					new Dictionary<string, AmqpChannelSetting>() { { testMessage.GetType().Name, _channelSetting } });
			Assert.Throws<ObjectDisposedException>(actual);
		}
			   
		[Fact]
		public void RabbitHost_RegisterSubscribe_AffterDispose()
		{
			var factoryMock = new RabbitFactoryMocks();
			IRabbitClient rabbitClient = new RabbitHost(factoryMock, new LoggerFake<RabbitHost>());
			(rabbitClient as RabbitHost).Dispose();
			Action actual = () => {
				rabbitClient.RegisterSubscribe<String>(_hostSetting, async (message) => { return CommunicationStatus.Empty; });
			};
			Assert.Throws<ObjectDisposedException>(actual);
		}

		[Fact]
		public void RabbitHost_RegisterSubscribeAndAndChannelSettings_AffterDispose()
		{
			var testMessage = "Test message is ok";
			var factoryMock = new RabbitFactoryMocks();
			IRabbitClient rabbitClient = new RabbitHost(factoryMock, new LoggerFake<RabbitHost>());
			(rabbitClient as RabbitHost).Dispose();
			Action actual = () =>
			{
				rabbitClient.RegisterSubscribe<String>(_hostSetting, _channelSetting, async (message) => { return CommunicationStatus.Empty; });
			};
			Assert.Throws<ObjectDisposedException>(actual);
		}
		
		[Fact]
		public void RabbitHost_StartAsync_AffterDispose()
		{
			var factoryMock = new RabbitFactoryMocks();
			IRabbitHost rabbitClient = new RabbitHost(factoryMock, new LoggerFake<RabbitHost>());
			(rabbitClient as RabbitHost).Dispose();
			var cancellationToken = new CancellationTokenSource();
			Action actual = () => rabbitClient.StartAsync(cancellationToken.Token);
			Assert.Throws<ObjectDisposedException>(actual);
		}

		[Fact]
		public async Task RabbitHost_Dispose_ForConnect()
		{
			var testMessage = "Test message is ok";
			var factoryMock = new RabbitFactoryMocks();
			IRabbitClient rabbitClient = new RabbitHost(factoryMock, new LoggerFake<RabbitHost>());

			IPublishChannel channel = null;
			await Task.Delay(0).Delay(() =>
			{
				//await open Channel
				channel = rabbitClient.Channel(_hostSetting,
					new Dictionary<string, AmqpChannelSetting>() { { testMessage.GetType().Name, _channelSetting } });
				return channel != null;
			}, 1000);

			IRabbitConnect connect = factoryMock.GetConnect(_hostSetting);
			(rabbitClient as RabbitHost).Dispose();
			Assert.True((connect as RabbitConnect).IsDisposed);
		}

		[Fact]
		public async Task RabbitHost_Dispose_ForChannel()
		{
			var testMessage = "Test message is ok";
			var factoryMock = new RabbitFactoryMocks();
			IRabbitClient rabbitClient = new RabbitHost(factoryMock, new LoggerFake<RabbitHost>());

			IPublishChannel channel = null;
			await Task.Delay(0).Delay(() =>
			{
				//await open Channel
				channel = rabbitClient.Channel(_hostSetting,
					new Dictionary<string, AmqpChannelSetting>() { { testMessage.GetType().Name, _channelSetting } });
				return channel != null;
			}, 1000);

			(rabbitClient as RabbitHost).Dispose();
			Assert.True((channel as RabbitChannel).IsDisposed);
		}
	}
}
