﻿using Rabbit.ClientService.Models;
using Rabbit.ClientService.Test.Mocks;
using RabbitMQ.Client;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Tools.UnitTest.Extension;
using Tools.UnitTest.Fakes;
using Xunit;

namespace Rabbit.ClientService.Test.Tests
{
	public class FactoryTest
	{
		AmqpHostSetting HostSetting
		{
			get
			{
				return new AmqpHostSetting(hostName: "test-host-FactoryTest", port: 1111, userName: "test-user", password: "test-pass", requestedHeartbeat: 1);
			}
		}

		AmqpChannelSetting ChannelSetting
		{
			get
			{
				return new AmqpChannelSetting(
					exchange: new AmqpExchangeSetting(name: "ex-p", type: "Fanout", autoDelete: false),
					routingKey: "*",
					queue: new AmqpQueueSetting(name: "q", autoDelete: false));
			}
		}

		public FactoryTest()
		{ }
		
		private Factory<ConnectionFactory> CreateFactoryMock()
		{
			return new Factory<ConnectionFactory>(new LoggerFake<Factory<ConnectionFactory>>());
		}
		
		[Fact]
		public void RabbitFactory_GetConnect_IfHostSetting_IsNull()
		{
			IRabbitConnectFactory factory = CreateFactoryMock();
			Action actual = () => factory.GetConnect(null);
			Assert.Throws<ArgumentNullException>(actual);
		}

		[Fact]
		public void RabbitFactory_GetConnect_AffterDispose()
		{
			var factory = CreateFactoryMock();
			factory.Dispose();
			Action actual = () => factory.GetConnect(new AmqpHostSetting() { });
			Assert.Throws<ObjectDisposedException>(actual);
		}
		
		[Fact]
		public void RabbitFactory_GetChannel_IfHostSetting_IsNull()
		{
			IChannelFactory factory = CreateFactoryMock();
			Action actual = () => factory.Channel(null);
			Assert.Throws<ArgumentNullException>(actual);
		}

		[Fact]
		public void RabbitFactory_GetChannel_AffterDispose()
		{
			var factory = CreateFactoryMock();
			factory.Dispose();
			Action actual = () => factory.Channel(new AmqpHostSetting() { });
			Assert.Throws<ObjectDisposedException>(actual);
		}
		
		[Fact]
		public async Task RabbitFactory_Dispose_ForConnect()
		{
			var testMessage = "Test message is ok";
			var factoryMock = new RabbitFactoryMocks();
			IRabbitClient rabbitClient = new RabbitHost(factoryMock, new LoggerFake<RabbitHost>());

			IPublishChannel channel = null;
			await Task.Delay(0).Delay(() =>
			{
				//await open Channel
				channel = rabbitClient.Channel(HostSetting,
					new Dictionary<string, AmqpChannelSetting>() { { testMessage.GetType().Name, ChannelSetting } });
				return channel != null;
			}, 1000);
			
			IRabbitConnect connect = factoryMock.GetConnect(HostSetting);
			factoryMock.Dispose();
			Assert.True((connect as RabbitConnect).IsDisposed);
		}

		[Fact]
		public async Task RabbitFactory_Dispose_ForChannel()
		{
			var testMessage = "Test message is ok";
			var factoryMock = new RabbitFactoryMocks();
			IRabbitClient rabbitClient = new RabbitHost(factoryMock, new LoggerFake<RabbitHost>());

			IPublishChannel channel = null;
			await Task.Delay(0).Delay(() =>
			{
				//await open Channel
				channel = rabbitClient.Channel(HostSetting,
					new Dictionary<string, AmqpChannelSetting>() { { testMessage.GetType().Name, ChannelSetting } });
				return channel != null;
			}, 1000);

			factoryMock.Dispose();
			Assert.True((channel as RabbitChannel).IsDisposed);
		}
	}
}
