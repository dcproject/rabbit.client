﻿using Rabbit.ClientService.Models;
using Rabbit.ClientService.Test.Mocks;
using System.Collections.Generic;
using System.Threading.Tasks;
using Tools.UnitTest.Fakes;
using Tools.UnitTest.Extension;
using Xunit;
using RabbitMQ.Fakes.models;

namespace Rabbit.ClientService.Test.Tests
{
	public class Publish
	{
		AmqpHostSetting HostSetting
		{
			get
			{
				return new AmqpHostSetting(hostName: "test-host-2", port: 1111, userName: "test-user", password: "test-pass", requestedHeartbeat: 1);
			}
		}

		AmqpChannelSetting ChannelSetting
		{
			get
			{
				return new AmqpChannelSetting(
					exchange: new AmqpExchangeSetting(name: "ex-p", type: "Fanout", autoDelete: false),
					routingKey: "*",
					queue: new AmqpQueueSetting(name: "q", autoDelete: false));
			}
		}

		[Fact]
		public async Task JustPublish_Test()
		{
			var testMessage = "Test message is ok";
			var rabbitMock = new RabbitFactoryMocks();
			IRabbitClient rabbitClient = new RabbitHost(rabbitMock, new LoggerFake<RabbitHost>());

			CommunicationStatus actualRresult = CommunicationStatus.Empty;
			IPublishChannel channel = null;
			await Task.Delay(0).Delay(() =>
			{
				//await open Channel
				channel = rabbitClient.Channel(HostSetting,
					new Dictionary<string, AmqpChannelSetting>() { { testMessage.GetType().Name, ChannelSetting } });
				return channel != null;
			}, 1000);
			await Task.Delay(0).Delay(() =>
				{
					actualRresult = channel.Publish(testMessage);
					return actualRresult == CommunicationStatus.Successfully;
				}, 1000);
			Assert.Equal(CommunicationStatus.Successfully, actualRresult);
		}

		[Fact]
		public async Task PublishMassage_Test()
		{
			var testMessage = "Test message is ok";
			var rabbitMock = new RabbitFactoryMocks();
			IRabbitClient rabbitClient = new RabbitHost(rabbitMock, new LoggerFake<RabbitHost>());

			int actualMassageCount = 0;
			IPublishChannel channel = null;
			await Task.Delay(0).Delay(() =>
			{
				//await open Channel
				channel = rabbitClient.Channel(HostSetting,
					new Dictionary<string, AmqpChannelSetting>() { { testMessage.GetType().Name, ChannelSetting } });
				return channel != null;
			}, 1000);
			channel.Publish(testMessage);
			await Task.Delay(0).Delay(() =>
			{
				rabbitMock.FakeFactory.Server.Exchanges.TryGetValue(ChannelSetting.Exchange.Name, out Exchange actualExchange);
				return (actualMassageCount = actualExchange?.Messages?.Count ?? 0) > 0;
			}, 1000);
			Assert.True(actualMassageCount > 0);
		}

	}
}
