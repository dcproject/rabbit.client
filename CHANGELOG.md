# Change Log
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [major.minor.build-revision] - {yyyy-MM-dd}
###ADDED: Added for new features.
###DEPRECATED: Deprecated for soon-to-be removed features.
###CHANGED: Changed for changes in existing functionality.
###FIXED: Fixed for any bug fixes.
###REMUVED: Removed for now removed features.
###SECURITY: Security in case of vulnerabilities.
#* [Task-Number](url): about

## [Unreleased]

# NEXT

## [1.0.0]   2020-11-14
###ADDED
#* [T-0001]() Add project Rabbit.ClientService with Tests and dependents (RebbitMQ.Fakes, Tools.UnitTest)