REM# Initialize sonar-scaner 
dotnet-sonarscanner begin /k:"Rabbit.ClientServicce" /d:sonar.cs.opencover.reportsPaths="Rabbit.ClientService.Test\coverage.opencover.xml" /d:sonar.coverage.exclusions="**Test*.cs"
REM# Build solution by sonar-scaner
dotnet build Rabbit.sln /t:Rebuild
dotnet-sonarscanner end 

pause
