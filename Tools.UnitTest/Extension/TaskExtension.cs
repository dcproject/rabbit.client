﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Threading.Tasks;

namespace Tools.UnitTest.Extension
{
	[ExcludeFromCodeCoverage]
	public static class TaskExtension
	{
		public static async Task Delay(this Task task, Func<bool> brakeCondition, int delay)
		{
			var checkDelay = 10;
			await task;
			await Task.Run(async () => {
				for (var i = delay/checkDelay; !brakeCondition() && i > 0; i--)
				{
					await Task.Delay(checkDelay);
				}
			});
			return;
		}

		public static async Task Delay(this Task task, Func<Task<bool>> brakeCondition, int delay)
		{
			var checkDelay = 10;
			await task;
			await Task.Run(async () => {
				bool isBreake = await brakeCondition();
				for (var i = delay / checkDelay; !isBreake && i > 0; i--)
				{
					await Task.Delay(checkDelay);
				}
			});
			return;
		}
	}
}
