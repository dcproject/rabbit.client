﻿using Newtonsoft.Json;
using System.Diagnostics.CodeAnalysis;
using System.Net;
using System.Net.Http;
using System.Text;

namespace Tools.UnitTest.Mocks
{
	[ExcludeFromCodeCoverage]
	public class HttpResponseMessageMock : HttpResponseMessage
	{
		public HttpResponseMessageMock()
		{
		}

		public HttpResponseMessageMock(HttpStatusCode statusCode) : base(statusCode)
		{
		}

		public HttpRequestMessage Request { get; set; }
		
		public HttpResponseMessageMock SetContent<T>(T content)
		{
			this.Content = new StringContent(
				content: JsonConvert.SerializeObject(content),
				mediaType: "application/json",
				encoding: Encoding.UTF8);
			return this;
		}
	}
}
