﻿using Rabbit.ClientService;
using Rabbit.ClientService.Events;
using Rabbit.ClientService.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Threading;
using System.Threading.Tasks;

namespace Tools.UnitTest.Fakes
{
	[ExcludeFromCodeCoverage]
	public class RabbitHostFake : IRabbitClient
	{
		public IPublishChannel Channel(AmqpHostSetting hostSetting)
		{
			throw new NotImplementedException();
		}

		public IPublishChannel Channel(AmqpHostSetting hostSetting, IDictionary<string, AmqpChannelSetting> setChannelSettings)
		{
			throw new NotImplementedException();
		}

		public void Dispose()
		{
			throw new NotImplementedException();
		}

		public CommunicationStatus RegisterSubscribe<T>(AmqpHostSetting hostSettings, Func<BasicDeliverEventArgs<T>, Task<CommunicationStatus>> receiveHandler, bool rewrait = true)
		{
			throw new NotImplementedException();
		}

		public CommunicationStatus RegisterSubscribe<T>(AmqpHostSetting hostSettings, AmqpChannelSetting channelSetting, Func<BasicDeliverEventArgs<T>, Task<CommunicationStatus>> receiveHandler, bool rewrait = true)
		{
			throw new NotImplementedException();
		}

		public Task StartAsync(CancellationToken cancellationToken)
		{
			throw new NotImplementedException();
		}

		public Task StopAsync(CancellationToken cancellationToken)
		{
			throw new NotImplementedException();
		}
	}
}
