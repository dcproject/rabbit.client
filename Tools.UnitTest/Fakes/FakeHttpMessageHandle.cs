﻿using System.Diagnostics.CodeAnalysis;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Tools.UnitTest.Mocks;

namespace Tools.UnitTest.Fakes
{
	[ExcludeFromCodeCoverage]
	public class FakeHttpMessageHandler : HttpMessageHandler
	{
		readonly HttpResponseMessage response;
		
		public FakeHttpMessageHandler(HttpResponseMessage response)
		{
			this.response = response; 
		}

		protected override Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
		{
			var tcs = new TaskCompletionSource<HttpResponseMessage>();
			var mockResponse = response as HttpResponseMessageMock;
			if (mockResponse != null) {
				mockResponse.Request = request;
			}

			tcs.SetResult(response);

			return tcs.Task;
		}
	}
}
