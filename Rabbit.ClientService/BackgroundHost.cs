﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Rabbit.ClientService.Extension;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Rabbit.ClientService
{
	internal class BackgroundHost : BackgroundService
	{
		private readonly ILogger _logger;
		private readonly IServiceProvider _servicesProvider;
		private IRabbitHost _rabbitHost = null;

		public BackgroundHost(IServiceProvider servicesProvider)
		{
			_logger = servicesProvider.GetRequiredService<ILogger<BackgroundHost>>();
			_servicesProvider = servicesProvider ?? throw new ArgumentNullException(nameof(servicesProvider));
			_logger.Log(this.GetType(), this.GetType().Name, this);
		}

		#region BackgroundService
		protected override Task ExecuteAsync(CancellationToken stoppingToken)
		{
			_logger.Log(this.GetType(), "ExecuteAsync", stoppingToken);
			if (IsDisposed)
			{
				throw new ObjectDisposedException(typeof(BackgroundHost).Name).Log(_logger);
			}
			_rabbitHost = _rabbitHost ?? _servicesProvider.GetService<IRabbitHost>();
			return _rabbitHost?.StartAsync(stoppingToken) ?? Task.CompletedTask;
		}

		public override Task StopAsync(CancellationToken cancellationToken)
		{
			if (IsDisposed)
			{
				throw new ObjectDisposedException(typeof(BackgroundHost).Name).Log(_logger);
			}
			return Task.WhenAll(new[] {
				_rabbitHost?.StopAsync(cancellationToken) ?? Task.CompletedTask,
				base.StopAsync(cancellationToken)
			});
		}

		#endregion

		#region IDisposable Support

		public bool IsDisposed { get { return _disposedValue; } }

		private bool _disposedValue = false;

		~BackgroundHost()
		{
			Dispose();
		}

		public override void Dispose()
		{
			_logger.Log(this.GetType(), "Dispose", this);
			if (_disposedValue)
			{
				return;
			}
			_disposedValue = true;
			(_rabbitHost as IDisposable).Dispose();
			base.Dispose();
		}

		#endregion

	}
}
