﻿using Rabbit.ClientService.Events;
using Rabbit.ClientService.Models;
using System;
using System.Threading.Tasks;

namespace Rabbit.ClientService
{
	public interface IRabbitClient : IPublishChannelFactory
	{
		CommunicationStatus RegisterSubscribe<T>(AmqpHostSetting hostSettings, Func<BasicDeliverEventArgs<T>, Task<CommunicationStatus>> receiveHandler, bool rewrait = true);
		CommunicationStatus RegisterSubscribe<T>(AmqpHostSetting hostSettings, AmqpChannelSetting channelSetting, Func<BasicDeliverEventArgs<T>, Task<CommunicationStatus>> receiveHandler, bool rewrait = true);
	}
}
