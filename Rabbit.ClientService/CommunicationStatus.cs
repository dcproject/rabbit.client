﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Rabbit.ClientService
{
	public enum CommunicationStatus
	{
		Empty = 0,
		Successfully = 1,
		FailingDelivery = 2,
		Duplicate = 3,
		Undefined = 4
	}
}
