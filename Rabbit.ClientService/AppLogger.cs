﻿using Microsoft.Extensions.Logging;
using RabbitMQ.Client;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Text;

namespace Rabbit.ClientService
{
	internal static class AppLogger
	{
		public static ILoggerFactory LoggerFactory { get; set; }

		public static ILogger Logger<T>()
		{
			return Logger(typeof(T));
		}

		public static ILogger Logger(Type type)
		{
			if (LoggerFactory == null)
			{
				return null;
			}
			int key = type.GetHashCode();
			ILogger logger;

			if (Loggers.TryGetValue(key, out logger))
			{
				return logger;
			}
			return Loggers?.GetOrAdd(key, LoggerFactory.CreateLogger(type));
		}

		internal static ConcurrentDictionary<int, ILogger> Loggers { get; } = new ConcurrentDictionary<int, ILogger>();
	}
}
