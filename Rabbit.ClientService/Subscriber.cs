﻿using Microsoft.Extensions.Logging;
using Rabbit.ClientService.Extension;
using Rabbit.ClientService.Models;
using RabbitMQ.Client.Events;
using System;
using System.Threading;

namespace Rabbit.ClientService
{
	internal class Subscriber
	{
		public AmqpChannelSetting ChannelSetting { get; }
		public EventHandler<BasicDeliverEventArgs> Receiving { get; }
		public string ConsumerTag { get; }
		public CommunicationStatus Status { get; set; }
		public Timer RetrySubscribeScheduler { get; set; }

		public Subscriber(EventHandler<BasicDeliverEventArgs> receiving, AmqpChannelSetting channelSetting, string consumerTag, ILogger logger)
		{
			Receiving = receiving ?? throw new ArgumentNullException(nameof(receiving)).Log(logger);
			ChannelSetting = channelSetting ?? throw new ArgumentNullException(nameof(channelSetting)).Log(logger);
			ConsumerTag = consumerTag ?? throw new ArgumentNullException(nameof(consumerTag)).Log(logger);
		}

		public override string ToString()
		{
			return $"({this.GetType().Name}|{ConsumerTag}|{ChannelSetting})";
		}

		public override int GetHashCode()
		{
			return ConsumerTag.GetHashCode();
		}

		public override bool Equals(object obj)
		{
			var fooItem = obj as Subscriber;

			if (fooItem == null)
			{
				return false;
			}

			return fooItem.GetHashCode() == this.GetHashCode();
		}
	}
}
