﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Rabbit.ClientService.Events;
using Rabbit.ClientService.Extension;
using Rabbit.ClientService.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;

[assembly: InternalsVisibleTo("Rabbit.ClientService.Test")]
namespace Rabbit.ClientService
{
	internal class RabbitHost : IRabbitHost
	{
		private readonly IChannelFactory _rabbitFactory;
		private readonly ILogger _logger;
		private readonly HashSet<IChannel> _subscribesChannel = new HashSet<IChannel>();

		public RabbitHost(IChannelFactory rabbitFactory, ILogger<RabbitHost> logger)
		{
			_logger = logger;
			_rabbitFactory = rabbitFactory ?? throw new ArgumentNullException(nameof(rabbitFactory)).Log(_logger);
			_logger.Log(this.GetType(), this.GetType().Name, rabbitFactory);
		}

		#region IRabbitClient
		public IPublishChannel Channel(AmqpHostSetting hostSetting)
		{
			if (IsDisposed)
			{
				throw new ObjectDisposedException(typeof(IRabbitConnect).Name).Log(_logger);
			}
			return _rabbitFactory.Channel(hostSetting);
		}

		public IPublishChannel Channel(AmqpHostSetting hostSetting, IDictionary<string, AmqpChannelSetting> setChannelSettings)
		{
			if (IsDisposed)
			{
				throw new ObjectDisposedException(typeof(IRabbitConnect).Name).Log(_logger);
			}
			return _rabbitFactory.Channel(hostSetting, setChannelSettings);
		}

		public CommunicationStatus RegisterSubscribe<T>(AmqpHostSetting hostSettings, Func<BasicDeliverEventArgs<T>, Task<CommunicationStatus>> receiveHandler, bool rewrait = true)
		{
			if (IsDisposed)
			{
				throw new ObjectDisposedException(typeof(IRabbitConnect).Name).Log(_logger);
			}
			IChannel channel = _rabbitFactory.Channel(hostSettings);
			_subscribesChannel.Add(channel);
			return channel.Subscribe(receiveHandler, rewrait);
		}

		public CommunicationStatus RegisterSubscribe<T>(AmqpHostSetting hostSettings, AmqpChannelSetting channelSetting, Func<BasicDeliverEventArgs<T>, Task<CommunicationStatus>> receiveHandler, bool rewrait = true)
		{
			if (IsDisposed)
			{
				throw new ObjectDisposedException(typeof(IRabbitConnect).Name);
			}
			IChannel channel = _rabbitFactory.Channel(hostSettings);
			_subscribesChannel.Add(channel);
			return channel.Subscribe(receiveHandler, channelSetting, rewrait);
		}
		#endregion


		#region BackgroundService
		public Task StartAsync(CancellationToken cancellationToken)
		{
			_logger.Log(this.GetType(), "StartAsync", cancellationToken);
			if (IsDisposed)
			{
				throw new ObjectDisposedException(typeof(IRabbitConnect).Name).Log(_logger);
			}
			foreach (var channel in _subscribesChannel)
			{
				channel?.SubscribeRestart();
			}
			return Task.CompletedTask;
		}

		public Task StopAsync(CancellationToken cancellationToken)
		{
			_logger.Log(this.GetType(), "StopAsync", cancellationToken);
			if (IsDisposed)
			{
				throw new ObjectDisposedException(typeof(IRabbitConnect).Name).Log(_logger);
			}
			foreach (var channel in _subscribesChannel)
			{
				channel?.Unsubscribe();
			}
			return Task.CompletedTask;
		}

		#endregion

		#region IDisposable Support
		public bool IsDisposed { get { return _disposedValue; } }

		private bool _disposedValue = false;

		protected virtual void Dispose(bool disposing)
		{
			_logger.Log(this.GetType(), "Dispose", disposing);
			if (_disposedValue)
			{
				return;
			}
			_disposedValue = true;
			_subscribesChannel?.Clear();
			(_rabbitFactory as IDisposable)?.Dispose();
		}

		~RabbitHost()
		{
			Dispose(false);
		}

		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}
		#endregion

	}
}
