﻿namespace Rabbit.ClientService.Models
{
	public class AmqpQueueSetting
	{
		/// <summary>
		/// Queue name
		/// </summary>
		public string Name { get; private set; }
		/// <summary>
		/// Switch auto delete for messages of queue 
		/// </summary>
		public bool AutoDelete { get; private set; } = false;
		///// <summary>
		///// 
		///// </summary>
		public bool Durable { get; private set; } = true;

		public AmqpQueueSetting()
		{
		}

		public AmqpQueueSetting(string name, bool autoDelete)
		{
			Name = name;
			AutoDelete = autoDelete;
		}

		public AmqpQueueSetting(string name, bool autoDelete, bool durable) : this(name, autoDelete)
		{
			Durable = durable;
		}

		public override string ToString()
		{
			return $"{Name}|{AutoDelete.ToString().ToLower()}|{Durable.ToString().ToLower()}";
		}

		public override int GetHashCode()
		{
			return this.ToString().GetHashCode();
		}

		public override bool Equals(object obj)
		{
			var fooItem = obj as AmqpQueueSetting;

			if (fooItem == null)
			{
				return false;
			}

			return fooItem.GetHashCode() == this.GetHashCode();
		}
	}
}
