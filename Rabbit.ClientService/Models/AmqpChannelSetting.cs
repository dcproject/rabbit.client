﻿namespace Rabbit.ClientService.Models
{
	public class AmqpChannelSetting
	{
		/// <summary>
		/// Use routing key value for filter messages go to the queue
		/// </summary>
		public string RoutingKey { get; private set; } = "*";

		public AmqpExchangeSetting Exchange { get; private set; }

		public AmqpQueueSetting Queue { get; private set; }

		public string ErrorChannelKey { get; set; }

		public ushort RetriesDelay { get; set; }

		public ushort RetriesCount { get; set; }


		public AmqpChannelSetting()
		{

		}

		public AmqpChannelSetting(AmqpExchangeSetting exchange, AmqpQueueSetting queue, string routingKey) : this(exchange, queue, routingKey, 0, 0)
		{
		}

		public AmqpChannelSetting(AmqpExchangeSetting exchange, AmqpQueueSetting queue, string routingKey, ushort retriesDelay, ushort retriesCount)
		{
			Exchange = exchange;
			Queue = queue;
			RoutingKey = routingKey;
			RetriesDelay = retriesDelay;
			RetriesCount = retriesCount;
		}

		public override string ToString()
		{
			return $"{Exchange}|{RoutingKey}|{Queue}";
		}

		public override int GetHashCode()
		{
			return this.ToString().GetHashCode();
		}

		public override bool Equals(object obj)
		{
			var fooItem = obj as AmqpQueueSetting;

			if (fooItem == null)
			{
				return false;
			}

			return fooItem.GetHashCode() == this.GetHashCode();
		}
	}
}
