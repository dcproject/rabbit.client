﻿using System.Collections.Generic;

namespace Rabbit.ClientService.Models
{
	public class AmqpHostSetting
	{
		public string HostName { get; private set; }
		public int Port { get; private set; }
		public string UserName { get; private set; }
		public string Password { get; private set; }
		public ushort RequestedHeartbeat { get; private set; }
		public long RetriesDelay { get; set; }
		public long RetriesCount { get; set; }

		public IDictionary<string, AmqpChannelSetting> ChannelsConfigurations { get; set; }

		public AmqpHostSetting()
		{
		}

		public AmqpHostSetting(string hostName, int port, string userName, string password, ushort requestedHeartbeat)
		{
			HostName = hostName;
			Port = port;
			UserName = userName;
			Password = password;
			RequestedHeartbeat = requestedHeartbeat;
		}

		public override string ToString()
		{
			return $"({HostName}:{Port}|{UserName}/{Password.GetHashCode()}|{RequestedHeartbeat})";
		}

		public override int GetHashCode()
		{
			return this.ToString().GetHashCode();
		}

		public override bool Equals(object obj)
		{
			var fooItem = obj as AmqpHostSetting;

			if (fooItem == null)
			{
				return false;
			}

			return fooItem.GetHashCode() == this.GetHashCode();
		}
	}
}
