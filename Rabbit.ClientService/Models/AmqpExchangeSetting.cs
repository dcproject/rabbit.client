﻿namespace Rabbit.ClientService.Models
{
	public class AmqpExchangeSetting
	{
		private string _type = null; 

		/// <summary>
		/// Exchange name
		/// </summary>
		public string Name { get; private set; }
		
		/// <summary>
		/// Exchange type used for AMQP direct exchanges. Cane string value: 
		/// Direct = "direct" 
		/// Fanout = "fanout"
		/// Headers = "headers"
		/// Topic = "topic"
		/// </summary>
		public string Type
		{
			get { return _type; }
			private set { _type = value?.ToLower(); }
		}

		/// <summary>
		/// Switch auto delete for messages of exchange 
		/// </summary>
		public bool AutoDelete { get; private set; } = false;
		
		///// <summary>
		///// 
		///// </summary>
		public bool Durable { get; private set; } = true;

		public AmqpExchangeSetting()
		{

		}

		public AmqpExchangeSetting(string name, string type, bool autoDelete)
		{
			Name = name;
			Type = type;
			AutoDelete = autoDelete;
		}

		public AmqpExchangeSetting(string name, string type, bool autoDelete, bool durable) : this(name, type, autoDelete)
		{
			Durable = durable;
		}

		public override string ToString()
		{
			return $"{Name}|{Type}|{AutoDelete.ToString().ToLower()}|{Durable.ToString().ToLower()}";
		}

		public override int GetHashCode()
		{
			return this.ToString().GetHashCode();
		}

		public override bool Equals(object obj)
		{
			var fooItem = obj as AmqpExchangeSetting;

			if (fooItem == null)
			{
				return false;
			}

			return fooItem.GetHashCode() == this.GetHashCode();
		}
	}
}
