﻿using System;
using System.Text;

namespace Rabbit.ClientService.Models
{
	internal class AmqpChannelErrorInfo<T>
	{
		public DateTime CreateDate { get; set; }
		public string ErrorMessage { get; set; }
		public string Message { get; set; }

		public AmqpChannelErrorInfo(string errorMessage, string message)
		{
			CreateDate = DateTime.UtcNow;
			ErrorMessage = errorMessage;
			Message = message;
		}

		public override string ToString()
		{
			return new StringBuilder(typeof(T).FullName)
				.AppendLine(CreateDate.ToString())
				.AppendLine(ErrorMessage)
				.AppendLine(Message)
				.ToString();
		}
	}
}
