﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using RabbitMQ.Client;

namespace Rabbit.ClientService
{
	public static class RegisterServices
	{
		public static IServiceCollection RegRabbitServices(this IServiceCollection services)
		{
			IRabbitHost rabbitHost = null;
			SetupLogger(services);
			return
				services
				.AddSingleton<IRabbitHost>((provider) =>
				{
					if (rabbitHost == null || rabbitHost.IsDisposed)
					{
						var rabbitFactory = new Factory<ConnectionFactory>(provider.GetRequiredService<ILogger<Factory<ConnectionFactory>>>());
						rabbitHost = new RabbitHost(rabbitFactory, provider.GetRequiredService<ILogger<RabbitHost>>());
					}
					return rabbitHost;
				})
				.AddSingleton<IRabbitClient>((provider) => provider.GetService<IRabbitHost>())
				.AddHostedService<BackgroundHost>();
		}

		private static void SetupLogger(this IServiceCollection services)
		{
			var provider = services?.BuildServiceProvider();
			AppLogger.LoggerFactory = provider?.GetRequiredService<ILoggerFactory>();
		}
	}
}
