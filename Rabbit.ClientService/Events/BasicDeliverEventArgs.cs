﻿using Microsoft.Extensions.Logging;
using Rabbit.ClientService.Extension;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;

namespace Rabbit.ClientService.Events
{
	public class BasicDeliverEventArgs<T> : EventArgs
	{
		private readonly ILogger _logger;
		private readonly BasicDeliverEventArgs _rabbitEventArgs;

		public BasicDeliverEventArgs(BasicDeliverEventArgs rabbitEventArgs, ILogger logger)
		{
			_logger = logger ?? throw new ArgumentNullException(nameof(logger));
			_rabbitEventArgs = rabbitEventArgs ?? throw new ArgumentNullException(nameof(rabbitEventArgs)).Log(_logger);
		}

		private IBasicProperties Properties
		{
			get
			{
				return _rabbitEventArgs.BasicProperties;
			}
		}

		public long Timestamp
		{
			get
			{
				return Properties?.Timestamp.UnixTime ?? default(long);
			}
		}

		public string CorrelationId
		{
			get
			{
				return Properties?.CorrelationId;
			}
		}

		public string JsonValue
		{
			get
			{
				return _rabbitEventArgs.GetJsonMesageBody(_logger);
			}
		}

		public T Value
		{
			get
			{
				return _rabbitEventArgs.GetMesageBode<T>(_logger);
			}
		}
	}
}
