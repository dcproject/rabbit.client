﻿using System;
using System.Threading.Tasks;

namespace Rabbit.ClientService.Extension
{
	public static class TaskExtension
	{
		public static async Task Delay(this Task task, Func<bool> brakeCondition, int delay, int checkDelay)
		{
			if(checkDelay < 100)
			{
				checkDelay = 100;
			}
			await task;
			await Task.Run(async () => {
				for (var i = delay / checkDelay; !brakeCondition() && i > 0; i--)
				{
					await Task.Delay(checkDelay);
				}
			});
		}
	}
}
