﻿using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using RabbitMQ.Client.Events;
using System;
using System.Text;

namespace Rabbit.ClientService.Extension
{
	public static class BasicDeliverEventArgsExtension
	{
		public static string GetJsonMesageBody(this BasicDeliverEventArgs args, ILogger logger)
		{
			if (args == null)
			{
				throw new ArgumentNullException(nameof(args)).Log(logger);
			}
			return Encoding.UTF8.GetString(args?.Body ?? new byte[] { });
		}

		public static T GetMesageBode<T>(this BasicDeliverEventArgs args, ILogger logger)
		{
			return JsonConvert.DeserializeObject<T>(args.GetJsonMesageBody(logger));
		}
	}
}
