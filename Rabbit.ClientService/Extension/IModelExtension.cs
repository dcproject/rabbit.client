﻿using Microsoft.Extensions.Logging;
using Rabbit.ClientService.Models;
using RabbitMQ.Client;
using System;

namespace Rabbit.ClientService.Extension
{
	internal static class IModelExtension
	{
		public static void TryBasicReject(this IModel channel, ulong deliveryTag, bool requeue)
		{
			if (channel?.IsOpen ?? false)
			{
				channel.BasicReject(deliveryTag, requeue);
			}
		}

		public static void TryBasicAck(this IModel channel, ulong deliveryTag, bool requeue)
		{
			if (channel?.IsOpen ?? false)
			{
				channel.BasicAck(deliveryTag, requeue);
			}
		}

		public static CommunicationStatus ExchangeDeclare(this IModel modelChannal, AmqpExchangeSetting setting, ILogger logger)
		{
			if (setting == null)
			{
				throw new ArgumentNullException(nameof(setting)).Log(logger);
			}
			if (modelChannal == null || !modelChannal.IsOpen)
			{
				new ArgumentNullException(nameof(modelChannal)).Log(logger);
				return CommunicationStatus.FailingDelivery;
			}
			modelChannal.ExchangeDeclare(
					exchange: setting.Name,
					type: setting.Type?.ToLower(),
					durable: setting.Durable,
					autoDelete: setting.AutoDelete,
					arguments: null
				);
			return CommunicationStatus.Successfully;
		}

		public static CommunicationStatus QueueDeclare(this IModel modelChannal, AmqpQueueSetting setting, ILogger logger)
		{
			if (setting == null)
			{
				throw new ArgumentNullException(nameof(setting)).Log(logger);
			}
			if (modelChannal == null || !modelChannal.IsOpen)
			{
				new ArgumentNullException(nameof(modelChannal)).Log(logger);
				return CommunicationStatus.FailingDelivery;
			}
			modelChannal.QueueDeclare(
				queue: setting.Name,
				durable: setting.Durable,
				autoDelete: setting.AutoDelete,
				exclusive: false,
				arguments: null
			);
			return CommunicationStatus.Successfully;
		}

		public static CommunicationStatus QueueBind(this IModel modelChannal, AmqpChannelSetting setting, ILogger logger)
		{
			if (setting == null)
			{
				throw new ArgumentNullException(nameof(setting)).Log(logger);
			}
			if (modelChannal == null || !modelChannal.IsOpen)
			{
				new ArgumentNullException(nameof(modelChannal)).Log(logger);
				return CommunicationStatus.FailingDelivery;
			}
			if (modelChannal.IsOpen)
			{
				modelChannal.QueueBind(
					queue: setting.Queue?.Name,
					exchange: setting.Exchange?.Name,
					routingKey: (setting.RoutingKey),
					arguments: null
				);
			}
			return CommunicationStatus.Successfully;
		}

		public static void Remove(this IModel channel)
		{
			if (channel != null)
			{
				if (channel.IsOpen)
				{
					channel.Close();
				}
				channel.Dispose();
			}
		}
	}
}
