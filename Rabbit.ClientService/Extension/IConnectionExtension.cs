﻿using RabbitMQ.Client;

namespace Rabbit.ClientService.Extension
{
	public static class IConnectionExtension
	{
		public static void Remove(this IConnection connection)
		{
			if (connection != null)
			{
				if (connection.IsOpen)
				{
					connection.Close();
				}
				connection.Dispose();
			}
		}
	}
}
