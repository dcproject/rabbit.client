﻿using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace Rabbit.ClientService.Extension
{
	internal static class ExceptionExtension
	{
		[MethodImpl(MethodImplOptions.NoInlining)]
		public static TException Log<TException>(this TException exception, ILogger logger, LogLevel? level = null) where TException : Exception
		{
			var logLevel = level ?? LogLevel.Error;
			if (logger != null && logger.IsEnabled(logLevel))
			{
				var method = new StackTrace().GetMethodsInfo(1);
				var exType = JsonConvert.SerializeObject(typeof(TException));
				var exValue = exception?.ToString();
				logger.Log(logLevel, $"[@Error message: {exception?.Message}; \t\n @method:{method} \t\n @type: {exType}; \t\n @Error value: {exValue}; \t\n @context value: {exception};]", exception);
			}
			return exception;
		}

		[MethodImpl(MethodImplOptions.NoInlining)]
		public static TException Log<TException>(this TException exception, Type contextType, LogLevel? level = null) where TException : Exception
		{
			return exception.Log(AppLogger.Logger(contextType), level);
		}
	}
}
