﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace Rabbit.ClientService.Extension
{
	internal static class StackTraceExtension
	{
		public static string GetMethodsInfo(this StackTrace stackTrace)
		{
			var sfs = stackTrace.GetFrames();
			return sfs.Select(sf => $"{sf.GetMethod().Name}, {sf.GetFileName()}:{sf.GetFileLineNumber()};\t\t").Aggregate((s, ss) => s + ss);
		}

		public static string GetMethodsInfo(this StackTrace stackTrace, int position)
		{
			var sf = stackTrace.GetFrame(position);
			return $"{sf.GetMethod().Name}, {sf.GetFileName()}:{sf.GetFileLineNumber()};\t\t";
		}
	}
}
