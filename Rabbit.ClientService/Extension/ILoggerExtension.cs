﻿using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Diagnostics;

namespace Rabbit.ClientService.Extension
{
	internal static class ILoggerExtension
	{
		public static string Log<TArg, TLogger>(this TLogger logger, Type context, string message, TArg arg, LogLevel? level = null)
			where TLogger : class, ILogger
		{
			logger = logger ?? AppLogger.Logger(context) as TLogger;
			var logLevel = level ?? LogLevel.Information;
			if (logger != null && logger.IsEnabled(logLevel))
			{
				var method = new StackTrace().GetMethodsInfo(1);
				var methods = new StackTrace().GetMethodsInfo();
				var contextType = JsonConvert.SerializeObject(context);
				var argType = JsonConvert.SerializeObject(arg as Type ?? typeof(TArg));
				var argValue = string.Empty;
				try
				{
					argValue = arg is Type ? "" : JsonConvert.SerializeObject(arg);
				}
				catch (Exception ex)
				{
					ex.Log(logger, LogLevel.Warning);
				}
				logger.Log(logLevel, 
						"[@message: {message}; \t\n@methot:{method}; \t\n @context type: {contextType}; \t\n @type: {argType}; \t\n @value: {argValue}; \t\n@stack trace methots:{methods}; \t\n]",
						message, contextType, argType, argValue, method, methods);
			}
			return message;
		}
	}
}
