﻿using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Rabbit.ClientService.Events;
using Rabbit.ClientService.Extension;
using Rabbit.ClientService.Models;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using RabbitMQ.Client.Framing;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Rabbit.ClientService
{
	internal partial class RabbitChannel : IChannel
	{
		private readonly ILogger _logger;
		private readonly IRabbitConnect _connect;
		private readonly object _subscribersLoc = new { };
		private readonly HashSet<Subscriber> _subscribers = new HashSet<Subscriber>();
		private readonly ConcurrentDictionary<string, AmqpChannelSetting> _setSettings = new ConcurrentDictionary<string, AmqpChannelSetting>();

		public bool IsOpen
		{
			get
			{
				return _connect?.IsOpen ?? false;
			}
		}

		public RabbitChannel(IRabbitConnect connect, ILogger logger)
		{
			_logger = logger ?? throw new ArgumentNullException(nameof(logger));
			_connect = connect ?? throw new ArgumentNullException(nameof(connect)).Log(_logger);
			_connect.OnConnect += this.SubscribeRestart;
		}

		private CommunicationStatus TrySetModelChannal(IModel modelChannal, AmqpChannelSetting setting)
		{
			CommunicationStatus status;
			try
			{
				status = modelChannal.ExchangeDeclare(setting.Exchange, _logger);
				if (status != CommunicationStatus.Successfully)
				{
					return status;
				}
				status = modelChannal.QueueDeclare(setting.Queue, _logger);
				if (status != CommunicationStatus.Successfully)
				{
					return status;
				}
				status = modelChannal.QueueBind(setting, _logger);
				if (status != CommunicationStatus.Successfully)
				{
					return status;
				}
			}
			catch (Exception ex)
			{
				ex.Log(_logger);
				status = CommunicationStatus.Undefined;
			}
			return status;
		}

		public void AppChannelsSetting(IDictionary<string, AmqpChannelSetting> setSettings)
		{
			if (setSettings == null)
			{
				throw new ArgumentNullException(nameof(setSettings)).Log(_logger);
			}
			foreach (var setting in setSettings)
			{
				_setSettings.TryAdd(setting.Key, setting.Value);
			}
		}

		public CommunicationStatus Publish<T>(T message, BasicProperties properties = null)
		{
			return Publish(message, typeof(T).Name, properties);
		}

		public CommunicationStatus Publish<T>(T messag, string channelKey, BasicProperties properties = null)
		{
			AmqpChannelSetting setting;
			_setSettings.TryGetValue(channelKey, out setting);
			return Publish(messag, setting, properties);
		}

		public CommunicationStatus Publish<T>(T message, AmqpChannelSetting setting, BasicProperties properties = null)
		{
			string jsonMessage = (message == null) ? string.Empty : JsonConvert.SerializeObject(message);
			return Publish(jsonMessage, setting, properties);
		}

		public CommunicationStatus Publish(string message, AmqpChannelSetting setting, BasicProperties properties = null)
		{
			byte[] messageBodyBytes = System.Text.Encoding.UTF8.GetBytes(message ?? "");
			if (setting == null)
			{
				throw new ArgumentNullException(nameof(setting)).Log(_logger);
			}
			IModel channel = _connect.GetChannelModel("Publish");
			CommunicationStatus status = TrySetModelChannal(channel, setting);
			if (status != CommunicationStatus.Successfully)
			{
				return status;
			}
			channel.BasicPublish(
				exchange: setting.Exchange?.Name,
				routingKey: (setting.RoutingKey),
				basicProperties: properties,
				body: messageBodyBytes);
			return CommunicationStatus.Successfully;
		}

		public CommunicationStatus Subscribe<T>(Func<BasicDeliverEventArgs<T>, Task<CommunicationStatus>> receiving, bool rewrait = true)
		{
			return Subscribe(receiving, typeof(T).Name, rewrait);
		}

		public CommunicationStatus Subscribe<T>(Func<BasicDeliverEventArgs<T>, Task<CommunicationStatus>> receiving, string channelKey, bool rewrait = true)
		{
			AmqpChannelSetting setting;
			_setSettings.TryGetValue(channelKey, out setting);
			return Subscribe(receiving, setting, rewrait);
		}

		public CommunicationStatus Subscribe<T>(Func<BasicDeliverEventArgs<T>, Task<CommunicationStatus>> receiving, AmqpChannelSetting setting, bool rewrait = true)
		{
			if (receiving == null)
			{
				throw new ArgumentNullException(nameof(receiving)).Log(_logger);
			}
			if (setting == null)
			{
				throw new ArgumentNullException(nameof(setting)).Log(_logger);
			}
			return Subscribe(
					receiving: (sender, eventArgs) =>
					{
						ReceivedAsync(sender as EventingBasicConsumer, eventArgs, receiving);
					},
					setting: setting,
					consumerTag: GetConsumerTag(typeof(T).Name, setting),
					rewrait: rewrait
				);
		}

		private CommunicationStatus Subscribe(EventHandler<BasicDeliverEventArgs> receiving, AmqpChannelSetting setting, string consumerTag, bool rewrait = true)
		{
			return Subscribe(new Subscriber(receiving, setting, consumerTag, _logger), rewrait);
		}

		private CommunicationStatus Subscribe(Subscriber subscriber, bool rewrait = true)
		{
			if (subscriber == null)
			{
				throw new ArgumentNullException(nameof(subscriber)).Log(_logger);
			}
			if (rewrait)
			{
				Unsubscribe(subscriber);
			}
			lock (_subscribersLoc)
			{
				if (!_subscribers.Add(subscriber))
				{
					return CommunicationStatus.Duplicate;
				}
			}
			return SubscribeStart(subscriber);
		}

		private CommunicationStatus SubscribeStart(Subscriber subscriber)
		{
			_logger.Log(this.GetType(), subscriber.ToString(), subscriber);
			IModel channel = _connect.GetChannelModel(subscriber.ConsumerTag);
			subscriber.Status = TrySetModelChannal(channel, subscriber.ChannelSetting);
			if (subscriber.Status == CommunicationStatus.Successfully)
			{
				_logger.Log(this.GetType(), subscriber.ToString(), channel);
				var consumer = new EventingBasicConsumer(channel);
				consumer.Received += subscriber.Receiving;
				channel.BasicConsume(
						queue: subscriber.ChannelSetting.Queue.Name,
						consumerTag: subscriber.ConsumerTag,
						autoAck: false,
						consumer: consumer
					);
			}
			_logger.Log(this.GetType(), subscriber.ToString(), subscriber,
				subscriber.Status == CommunicationStatus.Successfully ? LogLevel.Information : LogLevel.Warning);
			return subscriber.Status;
		}

		private void Unsubscribe(Subscriber subscriber)
		{
			if (subscriber == null)
			{
				throw new ArgumentNullException(nameof(subscriber)).Log(_logger);
			}
			IModel channel = null;
			try
			{
				channel = _connect.GetChannelModel(subscriber.ConsumerTag);
				lock (_subscribersLoc)
				{
					if (_subscribers.Remove(subscriber) && channel != null && channel.IsOpen)
					{
						channel.BasicCancel(subscriber.ConsumerTag);
					}
				}
			}
			catch (Exception ex)
			{
				ex.Log(_logger, LogLevel.Warning);
			}
		}

		public void SubscribeRestart()
		{
			foreach (var subscriber in _subscribers.ToList())
			{
				Subscribe(subscriber, true);
			}
		}

		public void Unsubscribe()
		{
			foreach (var subscriber in _subscribers)
			{
				Unsubscribe(subscriber);
			}
		}

		private void ReceivedAsync<T>(EventingBasicConsumer sender, BasicDeliverEventArgs args, Func<BasicDeliverEventArgs<T>, Task<CommunicationStatus>> receiving)
		{
			if (sender == null)
			{
				throw new ArgumentNullException(nameof(sender)).Log(_logger);
			}
			Task.Run(async () =>
			{
				string jsonMessage = string.Empty;
				try
				{
					jsonMessage = args.GetJsonMesageBody(_logger);
					CommunicationStatus status = await receiving(new BasicDeliverEventArgs<T>(args, _logger));
					switch (status)
					{
						case CommunicationStatus.Successfully:
							sender.Model.TryBasicAck(args.DeliveryTag, false);
							return true;
						case CommunicationStatus.FailingDelivery:
							sender.Model.TryBasicReject(args.DeliveryTag, true);
							ReceivedPause<T>(sender.Model, args.ConsumerTag);
							var warningMessage = new AmqpChannelErrorInfo<T>("False receiving, consumer is inexistent.", jsonMessage);
							_logger.Log(this.GetType(), warningMessage.ToString(), receiving, LogLevel.Warning);
							TryPublishErrorBy(warningMessage);
							break;
					}
				}
				catch (Exception ex)
				{
					sender.Model.TryBasicReject(args.DeliveryTag, false);
					TryPublishErrorBy(new AmqpChannelErrorInfo<T>(ex.ToString(), jsonMessage));
				}
				return false;
			});
		}

		private void ReceivedPause<T>(IModel channel, string consumerTag)
		{
			channel.BasicCancel(consumerTag);
			Subscriber subscriber = null;
			Predicate<Subscriber> subscribeSearch = (s) =>
			{
				if (subscriber != null || s.ConsumerTag != consumerTag)
				{
					return false;
				}
				subscriber = s;
				subscriber.Status = CommunicationStatus.FailingDelivery;
				return true;
			};
			_subscribers.RemoveWhere(subscribeSearch);

			var warningMessage = new AmqpChannelErrorInfo<T>("Pause of receiving, but consumer is inexistent. ", consumerTag);
			_logger.Log(this.GetType(), warningMessage.ToString(), subscriber, LogLevel.Critical);
			TryPublishErrorBy(warningMessage);

			SartRetrySubscribe(subscriber);
		}

		private void SartRetrySubscribe(Subscriber subscriber)
		{
			if (subscriber == null || subscriber.RetrySubscribeScheduler != null)
			{
				return;
			}
			subscriber.RetrySubscribeScheduler = new Timer((callBack) =>
			{
				if (Subscribe(subscriber) == CommunicationStatus.Successfully)
				{
					subscriber.RetrySubscribeScheduler.Dispose();
					subscriber.RetrySubscribeScheduler = null;
				}
			}, null, 30000, 9999999);
		}

		private void TryPublishErrorBy<T>(AmqpChannelErrorInfo<T> errorInfo)
		{
			if (!_setSettings.TryGetValue(typeof(T).Name, out AmqpChannelSetting channelSetting))
			{
				return;
			}
			if (!_setSettings.TryGetValue(channelSetting.ErrorChannelKey, out AmqpChannelSetting errorChannelSetting))
			{
				return;
			}
			Publish(errorInfo.ToString(), errorChannelSetting);
		}

		private string GetConsumerTag(string prefixTeg, AmqpChannelSetting setting)
		{
			return $"{prefixTeg}{setting.GetHashCode().ToString()}";
		}

		public override int GetHashCode()
		{
			return _connect.GetHashCode();
		}

		public override bool Equals(object obj)
		{
			var fooItem = obj as RabbitChannel;

			if (fooItem == null)
			{
				return false;
			}

			return fooItem.GetHashCode() == this.GetHashCode();
		}
	}
}
