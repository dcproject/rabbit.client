﻿using Rabbit.ClientService.Events;
using Rabbit.ClientService.Models;
using System;
using System.Threading.Tasks;

namespace Rabbit.ClientService
{
	internal interface ISubscribeChannel
	{
		CommunicationStatus Subscribe<T>(Func<BasicDeliverEventArgs<T>, Task<CommunicationStatus>> receiving, bool rewrait = true);

		CommunicationStatus Subscribe<T>(Func<BasicDeliverEventArgs<T>, Task<CommunicationStatus>> receiving, string channelKey, bool rewrait = true);

		CommunicationStatus Subscribe<T>(Func<BasicDeliverEventArgs<T>, Task<CommunicationStatus>> receiving, AmqpChannelSetting setting, bool rewrait = true);

		void SubscribeRestart();

		void Unsubscribe();
	}
}
