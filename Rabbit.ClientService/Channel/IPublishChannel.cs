﻿using Rabbit.ClientService.Models;
using RabbitMQ.Client.Framing;
using System.Collections.Generic;

namespace Rabbit.ClientService
{
	public interface IPublishChannel
	{
		void AppChannelsSetting(IDictionary<string, AmqpChannelSetting> setSettings);

		bool IsOpen { get; }

		CommunicationStatus Publish<T>(T message, BasicProperties properties = null);

		CommunicationStatus Publish<T>(T messag, string channelKey, BasicProperties properties = null);
	}
}
