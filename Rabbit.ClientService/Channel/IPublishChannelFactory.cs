﻿using Rabbit.ClientService.Models;
using System.Collections.Generic;

namespace Rabbit.ClientService
{
	public interface IPublishChannelFactory
	{
		IPublishChannel Channel(AmqpHostSetting hostSetting);
		IPublishChannel Channel(AmqpHostSetting hostSetting, IDictionary<string, AmqpChannelSetting> setChannelSettings);
	}
}
