﻿using Rabbit.ClientService.Models;
using System.Collections.Generic;

namespace Rabbit.ClientService
{
	internal interface IChannelFactory
	{
		IChannel Channel(AmqpHostSetting hostSetting);
		IChannel Channel(AmqpHostSetting hostSetting, IDictionary<string, AmqpChannelSetting> setChannelSettings);
	}
}
