﻿using System;
using System.Linq;
using System.Threading.Tasks;

namespace Rabbit.ClientService
{
	internal partial class RabbitChannel : IDisposable
	{
		#region IDisposable Support

		public bool IsDisposed { get { return _disposedValue; } }

		private bool _disposedValue = false;

		protected virtual void Dispose(bool disposing)
		{
			if (_disposedValue)
			{
				return;
			}
			_disposedValue = true;
			if (_subscribers != null)
			{
				foreach (var subscriber in _subscribers.ToList())
				{
					Unsubscribe(subscriber);
				}
			}
		}

		~RabbitChannel()
		{
			Dispose(false);
		}

		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}
		#endregion

	}
}
