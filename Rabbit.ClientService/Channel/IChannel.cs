﻿namespace Rabbit.ClientService
{
	internal interface IChannel : IPublishChannel, ISubscribeChannel
	{
	}
}
