﻿using Microsoft.Extensions.Logging;
using Rabbit.ClientService.Extension;
using Rabbit.ClientService.Models;
using RabbitMQ.Client;
using RabbitMQ.Client.Exceptions;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;

namespace Rabbit.ClientService
{
	internal class Factory<TFactory> : IRabbitConnectFactory, IChannelFactory, IDisposable where TFactory : ConnectionFactory, new()
	{
		private readonly ILogger _logger;
		private readonly object _connectsLock = new { };
		private readonly HashSet<RabbitConnect> _connects = new HashSet<RabbitConnect>();
		private readonly object _channelsLock = new { };
		private readonly HashSet<RabbitChannel> _channels = new HashSet<RabbitChannel>();
		private readonly ConcurrentDictionary<int, IConnectionFactory> _connectionsFactories = new ConcurrentDictionary<int, IConnectionFactory>();

		#region IRabbitConnectFactory
		public Factory(ILogger logger)
		{
			_logger = logger ?? throw new ArgumentNullException(nameof(logger));
		}

		public IRabbitConnect GetConnect(AmqpHostSetting setting)
		{
			if (setting == null)
			{
				throw new ArgumentNullException(nameof(setting)).Log(_logger);
			}
			if (_disposedValue)
			{
				throw new ObjectDisposedException(typeof(IConnectionFactory).Name).Log(_logger);
			}
			RabbitConnect connect;
			lock (_connectsLock)
			{
				var hashCode = setting.GetHashCode();
				connect = _connects.FirstOrDefault(c => c.HashOfSetting == hashCode);
				if (connect == null)
				{
					connect = CreateConnect(setting);
					_connects.Add(connect);
				}
			}
			return connect;
		}

		private RabbitConnect CreateConnect(AmqpHostSetting setting)
		{
			if (_disposedValue)
			{
				throw new ObjectDisposedException(typeof(IConnectionFactory).Name).Log(_logger);
			}
			return new RabbitConnect(() => { return CreateConnection(setting); },
				setting.GetHashCode(), setting.ToString(), setting.RetriesDelay, setting.RetriesCount, _logger);
		}

		IConnection CreateConnection(AmqpHostSetting setting)
		{
			if (_disposedValue)
			{
				throw new ObjectDisposedException(typeof(IRabbitConnectFactory).Name).Log(_logger);
			}
			if (setting == null)
			{
				throw new ArgumentNullException(nameof(setting)).Log(_logger);
			}
			IConnection conect = null;
			bool retry = _connectionsFactories.TryGetValue(setting.GetHashCode(), out IConnectionFactory factory);
			do
			{
				if (!retry)
				{
					retry = false;
					factory = _connectionsFactories.GetOrAdd(setting.GetHashCode(), CreateConnectFactory(setting));
				}
				try
				{
					conect = factory.CreateConnection();
					retry = false;
				}
				catch (BrokerUnreachableException ex)
				{
					ex.Log(_logger, LogLevel.Warning);
					_connectionsFactories.TryRemove(setting.GetHashCode(), out factory);
				}
			} while (retry);
			_logger.Log(this.GetType(), setting.ToString(), conect, (conect != null && conect.IsOpen) ? LogLevel.Information : LogLevel.Error);
			return conect;
		}

		protected virtual IConnectionFactory CreateConnectFactory(AmqpHostSetting setting)
		{
			if (setting == null)
			{
				throw new ArgumentNullException(nameof(setting)).Log(_logger);
			}
			if (_disposedValue)
			{
				throw new ObjectDisposedException(typeof(IConnectionFactory).Name).Log(_logger);
			}
			return new TFactory()
			{
				HostName = setting.HostName,
				Port = setting.Port,
				UserName = setting.UserName,
				Password = setting.Password,
				RequestedHeartbeat = setting.RequestedHeartbeat
			};
		}
		#endregion

		#region IRabbitChannelFactory

		public IChannel Channel(AmqpHostSetting hostSetting)
		{
			if (hostSetting == null)
			{
				throw new ArgumentNullException(nameof(hostSetting)).Log(_logger);
			}
			if (_disposedValue)
			{
				throw new ObjectDisposedException(typeof(IConnectionFactory).Name).Log(_logger);
			}
			RabbitChannel channel;
			lock (_channelsLock)
			{
				var hashCode = hostSetting.GetHashCode();
				channel = _channels.FirstOrDefault(c => c.GetHashCode() == hashCode);
				if (channel == null)
				{
					channel = new RabbitChannel(GetConnect(hostSetting), _logger);
					_channels.Add(channel);
				}
			}
			return channel;
		}

		public IChannel Channel(AmqpHostSetting hostSetting, IDictionary<string, AmqpChannelSetting> setChannelSettings)
		{
			var channel = Channel(hostSetting);
			channel.AppChannelsSetting(setChannelSettings);
			return channel;
		}
		#endregion

		#region IDisposable Support
		private bool _disposedValue = false;

		protected virtual void Dispose(bool disposing)
		{
			if (_disposedValue)
			{
				return;
			}
			_disposedValue = true;
			if (_channels != null)
			{
				foreach (var channel in _channels)
				{
					channel.Dispose();
				}
				_channels?.Clear();
			}
			if (_connects != null)
			{
				foreach (var connect in _connects)
				{
					connect.Dispose();
				}
				_connects.Clear();
			}
		}

		~Factory()
		{
			Dispose(false);
		}

		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}
		#endregion
	}
}
