﻿using Microsoft.Extensions.Hosting;
using Rabbit.ClientService.Events;
using Rabbit.ClientService.Models;
using System;
using System.Threading.Tasks;

namespace Rabbit.ClientService
{
	internal interface IRabbitHost : IRabbitClient, IHostedService, IDisposable
	{
		bool IsDisposed { get; }
	}
}
