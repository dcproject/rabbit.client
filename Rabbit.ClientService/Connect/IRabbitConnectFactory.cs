﻿using Rabbit.ClientService.Models;

namespace Rabbit.ClientService
{
	internal interface IRabbitConnectFactory
	{
		IRabbitConnect GetConnect(AmqpHostSetting setting);
	}
}
