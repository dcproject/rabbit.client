﻿using Rabbit.ClientService.Extension;
using RabbitMQ.Client;
using System;
using System.Linq;

namespace Rabbit.ClientService
{
	internal partial class RabbitConnect : IDisposable
	{
		#region IDisposable Support

		public bool IsDisposed { get { return _disposedValue; } }

		private bool _disposedValue = false;

		protected virtual void Dispose(bool disposing)
		{
			if (_disposedValue)
			{
				return;
			}
			_disposedValue = true;
			_handlerTimeCounter?.Dispose();
			if (_channels != null)
			{
				foreach (var cannalKey in _channels.Keys.ToList())
				{
					_channels.TryRemove(cannalKey, out IModel channel);
					channel.Remove();
				}
			}
			_connect.Remove();
		}

		~RabbitConnect()
		{
			Dispose(false);
		}

		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}
		#endregion
	}
}
