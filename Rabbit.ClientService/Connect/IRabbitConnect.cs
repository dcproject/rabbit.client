﻿using RabbitMQ.Client;
using System;
using System.Threading.Tasks;

namespace Rabbit.ClientService
{
	internal interface IRabbitConnect : IDisposable
	{
		event Action OnConnect;

		int HashOfSetting { get; }

		bool IsOpen { get; }

		bool TryConnect();

		IModel GetChannelModel(string key);
		
		bool IsDisposed { get; }
	}
}
