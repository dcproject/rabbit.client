﻿using Microsoft.Extensions.Logging;
using Rabbit.ClientService.Extension;
using RabbitMQ.Client;
using System;
using System.Collections.Concurrent;
using System.Threading;

namespace Rabbit.ClientService
{
	internal partial class RabbitConnect : IRabbitConnect
	{
		private Timer _handlerTimeCounter = null;
		private readonly ILogger _logger;
		private readonly Func<IConnection> _createConnection;
		private readonly object _connectLock = new { };
		private IConnection _connect;
		private readonly ConcurrentDictionary<string, IModel> _channels = new ConcurrentDictionary<string, IModel>();

		private string _hostSettingInfo { get; }

		public string Name { get; } = typeof(IRabbitConnect).Namespace;
		public int HashOfSetting { get; }
		public long RetriesDelay { get; set; }
		public long RetriesCount { get; set; }

		private bool _isNeedResubscribe { get; set; } = true;

		public bool IsOpen
		{
			get
			{
				if (IsDisposed || _connect == null || !_connect.IsOpen)
				{
					return false;
				}
				return true;
			}
		}

		public event Action OnConnect;

		public RabbitConnect(Func<IConnection> connectionCreator, int hashOfSetting, string hostSettingInfo, long retriesDelay, long retriesCount, ILogger logger)
		{
			_logger = logger ?? throw new ArgumentNullException(nameof(logger));
			HashOfSetting = hashOfSetting;
			_hostSettingInfo = hostSettingInfo;
			_createConnection = connectionCreator ?? throw new ArgumentNullException(nameof(connectionCreator)).Log(_logger);
			RetriesDelay = retriesDelay > 0 ? retriesDelay : default(long);
			RetriesCount = retriesCount >= 0 ? retriesCount : default(long);
		}

		public bool TryConnect()
		{
			return TryConnect(RetriesDelay, RetriesDelay, RetriesCount);
		}

		private bool TryConnect(long startDelay, long period, long retries)
		{
			if (IsDisposed)
			{
				return false;
			}
			lock (_connectLock)
			{
				if (!IsOpen)
				{
					if (_connect != null)
					{
						_isNeedResubscribe = true;
						_connect.Remove();
					}
					_connect = _createConnection();
					if (!IsOpen)
					{
						SartRetryTryConnect(startDelay, period, retries);
					}
				}
			}
			if (IsOpen)
			{
				StopRetryTryConnect();
				if (_isNeedResubscribe)
				{
					try
					{
						Subscribe(_connect);
					}
					catch (Exception ex)
					{
						ex.Log(_logger, LogLevel.Warning);
						SartRetryTryConnect(startDelay, period, retries);
					}
				}
			}
			return IsOpen;
		}

		private void SartRetryTryConnect(long startDelay, long period, long retries)
		{
			if (_handlerTimeCounter == null && period > 0 && retries > 0)
			{
				_isNeedResubscribe = true;
				_handlerTimeCounter = new Timer((callBack) =>
				{
					TryConnect(startDelay, period, --retries);
					if (retries < 0)
					{
						StopRetryTryConnect();
					}
				}, null, startDelay, period);
			}
		}

		private void StopRetryTryConnect()
		{
			_handlerTimeCounter?.Dispose();
			_handlerTimeCounter = null;
		}

		private void Subscribe(IConnection connect)
		{
			if (connect == null)
			{
				throw new ArgumentNullException(nameof(connect)).Log(_logger);
			}
			_isNeedResubscribe = false;
			connect.ConnectionShutdown -= OnDisconect;
			connect.ConnectionBlocked += OnDisconect;
			connect.CallbackException -= OnDisconect;
			connect.CallbackException += OnDisconect;
			connect.ConnectionBlocked -= OnDisconect;
			connect.ConnectionShutdown += OnDisconect;
			OnConnect();
		}

		private void OnDisconect<T>(object sender, T e) where T : EventArgs
		{

			if (IsDisposed) return;
			_logger.Log(this.GetType(), _hostSettingInfo, e, LogLevel.Warning);
			TryConnect(RetriesDelay, RetriesDelay, RetriesCount);
		}

		public IModel GetChannelModel(string key)
		{
			if (IsDisposed)
			{
				throw new ObjectDisposedException(typeof(IRabbitConnect).Name).Log(_logger);
			}
			_channels.TryGetValue(key, out IModel channel);
			if (channel != null)
			{
				if (channel.IsOpen)
				{
					return channel;
				}
				_channels.TryRemove(key, out channel);
				channel.Remove();
			}
			if (TryConnect())
			{
				channel = _channels.GetOrAdd(key, _connect.CreateModel());
			}
			_logger.Log(this.GetType(), $"[{_hostSettingInfo} | {key}]", channel, channel != null && channel.IsOpen ? LogLevel.Information : LogLevel.Warning);
			return channel;
		}

		public override int GetHashCode()
		{
			return HashOfSetting.GetHashCode();
		}

		public override bool Equals(object obj)
		{
			var fooItem = obj as RabbitConnect;

			if (fooItem == null)
			{
				return false;
			}

			return fooItem.GetHashCode() == this.GetHashCode();
		}
	}
}
